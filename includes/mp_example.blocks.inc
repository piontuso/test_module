<?php

/**
 * Implements hook_block_info().
 */
function mp_example_block_info() {
  $blocks = array();
  $blocks['mp_example_guest_opinion_block'] = array(
    'info' => t('Guest opinion block (mp_example)'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function mp_example_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'mp_example_guest_opinion_block':
      $block['subject'] = 'Guest opinions';
      $block['content'] = _mp_example_guest_opinion_block();
      break;
  }
  return $block;
}

/**
 * Custom block content.
 *
 * @return string
 */
function _mp_example_guest_opinion_block() {
  $output = '';
  $opinions = _mp_example_get_opinions();
  $output .= _mp_example_generate_opinions_output($opinions);
  $output .= '<div id="magical-modal-link">' . l(t('Add opinion'), 'guest-opinion/add/nojs', array('attributes' => array('class' => 'ctools-use-modal'))) . '</div>';

  return $output;
}

/**
 * Function generates output for opinions.
 *
 * @param array $opinions
 *   Array of opinions.
 *
 * @return string
 */
function _mp_example_generate_opinions_output($opinions) {
  $output = '';
  $output  .= '<div id="guest-opinions">';
  foreach ($opinions as $key => $opinion) {
    $user = user_load($opinion->uid);
    $output .= '<p>' . $opinion->opinion;
    $output .= '</br><span>submitted by ' . $user->name . '</span>';
    $output .= '</p>';
  }
  $output .= '</div>';

  return $output;
}
