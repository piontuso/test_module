<?php

/**
 * @file
 * Implments mp_example forms.
 */

/**
 * Implements example form.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 *
 * @return array
 */
function mp_example_form($form, &$form_state) {
  $form = array();

  $form['guest_opinion'] = array(
    '#type' => 'fieldset',
    '#title' => t('Guest opinion'),
  );
  $form['guest_opinion']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
  );
  $form['guest_opinion']['opinion'] = array(
    '#type' => 'textarea',
    '#title' => t('Your opinion'),
  );
  $form['guest_opinion']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Publish'),
  );

  return $form;
}

/**
 * Implements example form validate function.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function mp_example_form_validate($form, &$form_state) {
  if (empty($form_state['values']['opinion'])) {
    form_set_error('opinion', 'The field can not be empty.');
  }
}

/**
 * Implements example form submit.
 *
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form state array.
 */
function mp_example_form_submit($form, &$form_state) {
  _mp_example_set_opinion($form_state['values']);
  $opinions = _mp_example_get_opinions();
  $output = _mp_example_generate_opinions_output($opinions);
  $form_state['ajax_commands'][] = ajax_command_replace('#guest-opinions', $output);
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
}
