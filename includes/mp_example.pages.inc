<?php

/**
 * @file
 * Implements mp_example pages.
 */

/**
 * Modal callback function.
 *
 * @param $ajax
 * @return array|mixed
 */
function _mp_example_modal_callback($ajax) {
  if ($ajax) {
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Add guest opinion'),
    );

    $output = ctools_modal_form_wrapper('mp_example_form', $form_state);

    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }

    print ajax_render($output);
    drupal_exit();
  }
  else {
    return drupal_get_form('mp_example_form');
  }
}
